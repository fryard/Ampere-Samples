# OCP Hardware

Customer Reference Board (CRB) Wiwynn platform for Altra and Altra Max.

```
https://www.opencompute.org
->Projects
-->Server
--->Details
---->Specifications and Designs
----->Marketplace Families
------>Server
```

[![Ampere Open Compute Specification Mt. Jade](docs/img/ampere_ocp_server_mt_jade.png)](https://www.opencompute.org/contributions?refinementList%5Bproject%5D%5B0%5D=Server&refinementList%5Bfamily%5D%5B0%5D=Server&refinementList%5Bcontributor%5D%5B0%5D=Ampere%20Computing&page=1&configure%5BfacetFilters%5D%5B0%5D=archived%3Afalse)

[Ampere Open Compute Specification Mt. Jade](https://www.opencompute.org/documents/open-compute-specification-mt-jade-rev-1-0-pdf-1)

[Ampere Open Compute GitHub Mt. Jade](https://github.com/opencomputeproject/OCP-Accepted-Ampere-Computing)

# Arm64 Option ROM Examples
## Flashrom AMD Radeon HD 5450 Video Card
[Radeon HD 5450 Option ROM](examples/flashrom)
# OCP Firmware Examples

## UEFI EDK2 Example (LinuxBoot, Tianocore, Tianocore Capsule) for Mt Jade

[![EDK2 example](examples/edk2/docs/img/ampere-linuxboot-edk2.png)](examples/edk2)

---
## OpenBMC for Mt Jade

[<img src="https://upload.wikimedia.org/wikipedia/commons/6/66/OpenBMC_logo.png" alt="OpenBMC-img" width="400"/>](examples/openbmc)

---

### Example Containers and Images

```
[linux-laptop openbmc]$ docker ps -a
CONTAINER ID   IMAGE                COMMAND                  CREATED          STATUS          PORTS     NAMES
7cb956ca6102   ampere.openbmc.img   "/root/share/build.sh"   15 minutes ago   Up 15 minutes             ampere.openbmc.cntnr
71f9e66af9f6   ampere.edk2.img      "/root/share/build.sh"   30 minutes ago   Up 30 minutes             ampere.edk2.cntnr

[linux-laptop openbmc]$ docker images
REPOSITORY           TAG               IMAGE ID       CREATED          SIZE
ampere.openbmc.img   latest            50a9c2206ee2   15 minutes ago   738MB
ampere.edk2.img      latest            b40247e22c19   30 minutes ago   3.62GB
ubuntu               bionic-20210930   5a214d77f5d7   2 months ago     63.1MB
centos               8                 5d0da3dc9764   2 months ago     231MB
```
---
## Docker Containers

Clean up your developer Docker installation starting fresh - don't do this on a production system

`docker stop $(docker ps -aq);docker rm $(docker ps -aq);docker rmi $(docker images -q);docker system prune -a -f`

### Getting Started

1. (a) Install Docker on an x86 Linux metal or VM.

*or*

1. (b) Use WSL2 with the Windows Docker CE installer

2. See example folders for container build and run syntax

3. Run the container and watch output

4. Look for `Example Results` collateral listed below

### Basic model for these examples

    * Clone repo with Dockerfile(s) and Compose files
    * Build containers with Compose
    * Run containers (development container/firmware build container)
    * Connect to the microservices and/or use the generated binaries to flash

### Example Results

I) For examples that build firmware images, look in the specific example directory for the final binary produced by the container.

II) For examples that build containers for JTAG debugging purposes (e.g. OpenOCD), you'll end up with a container that can be used to debug an Ampere Altra target.
