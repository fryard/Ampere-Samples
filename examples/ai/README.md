# Ampere Model Zoo Container Examples

## Onnx
[Onnx Runtime Benchmark Arm64 Example](README-onnxruntime.md)

## PyTorch
[PyTorch Benchmark Arm64/Intel/AMD Example](README-pytorch.md)

## Tensorflow
[Tensorflow Benchmark Arm64 Example](README-tensorflow.md)

# Onnx Models

List fp32 models inside the container example

`root@ampere.modelzoo.onnxruntime.cntnr:/ampere/mzd# python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f ort -b 1 --debug`

```
Available fp32 models:

  densenet_121
  mobilenet_v2
  resnet_50_v1
  shufflenet
  ssd_mobilenet_v2
  vgg_16
  yolo_v3
```

# PyTorch Models

List fp32 models inside the container example

`root@ampere.modelzoo.pytorch.cntnr:/ampere/mzd# python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug`

```
Available fp32 models:

  3d_unet_brats
  alexnet
  alpaca
  bert_base_graphcore
  bert_large_mlperf_squad
  densenet_121
  dlrm_debug
  dlrm_torchbench
  googlenet
  inception_v3
  mobilenet_v2
  mobilenet_v3_large
  mobilenet_v3_small
  nasnet_mobile
  resnet_18
  resnet_50_v1.5
  retinanet
  retinanet_torchvision
  roberta_base_squad
  shufflenet
  squeezenet
  ssd_vgg_16
  vgg_16
  whisper_base.en
  whisper_large
  whisper_medium.en
  whisper_small.en
  whisper_tiny.en
  yolo_v5_l
  yolo_v5_m
  yolo_v5_n
  yolo_v5_s
  yolo_v5_x
  yolo_v8_l
  yolo_v8_m
  yolo_v8_n
  yolo_v8_s
  yolo_v8_x
```

# Tensorflow Models

List fp32 models inside the container example

`root@ampere.modelzoo.tensorflow.cntnr:/ampere/mzd# python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f tf -b 1 --debug`

```
Available fp32 models:

  3d_unet_brats
  3d_unet_kits
  bert_base_c_squad
  bert_base_uc_squad
  bert_large_c_wwm_squad
  bert_large_mlperf_squad
  bert_large_uc_wwm_squad
  densenet_169
  distilbert_base_c_squad
  distilbert_base_uc_squad
  electra_large_squad
  inception_resnet_v2
  inception_v2
  inception_v3
  inception_v4
  longformer_base_squad
  mobilenet_v1
  mobilenet_v2
  nasnet_large
  nasnet_mobile
  resnet_101_v2
  resnet_50_v1.5
  resnet_50_v2
  roberta_base_squad
  squeezenet
  ssd_inception_v2
  ssd_mobilenet_v1
  ssd_mobilenet_v2
  ssd_resnet_34
  vgg_16
  vgg_19
  yolo_v4_tiny
```

# Benchmark Results

## Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files/`

```
csv_files/
└── arm64
    ├── ort@resnet_50_v1@fp32@ss.csv
    ├── pytorch@resnet_50_v1.5@fp32@ss.csv
    └── tf@resnet_50_v1.5@fp32@ss.csv
```

## Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── intel
    └── pytorch@resnet_50_v1.5@fp32@ss.csv
```

## AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── amd
    └── pytorch@resnet_50_v1.5@fp32@ss.csv
```

# Docker System Artifacts

## Images

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                              TAG       IMAGE ID       CREATED          SIZE
ampere.modelzoo.pytorch.arm64.img       latest    e399f43ecd44   34 minutes ago   5.85GB
ampere.modelzoo.tensorflow.arm64.img    latest    39ae40fff242   36 minutes ago   5.23GB
ampere.modelzoo.onnxruntime.arm64.img   latest    89755e0443de   58 minutes ago   6.52GB
```

### Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                          TAG       IMAGE ID       CREATED          SIZE
ampere.modelzoo.pytorch.intel.img   latest    88a3e9e56012   16 seconds ago   7.17GB
```

### AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                        TAG       IMAGE ID       CREATED              SIZE
ampere.modelzoo.pytorch.amd.img   latest    341b8b165ca9   About a minute ago   13.8GB
```

## Containers
### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                                   COMMAND                  CREATED          STATUS          PORTS     NAMES
6dce3b1cabfe   ampere.modelzoo.tensorflow.arm64.img    "bash /.aio_init/gre…"   7 seconds ago    Up 7 seconds              ampere.modelzoo.tensorflow.arm64.cntnr
cfce076f123a   ampere.modelzoo.pytorch.arm64.img       "bash /.aio_init/gre…"   24 seconds ago   Up 24 seconds             ampere.modelzoo.pytorch.arm64.cntnr
a93abfd2a03a   ampere.modelzoo.onnxruntime.arm64.img   "bash /.aio_init/gre…"   36 seconds ago   Up 36 seconds             ampere.modelzoo.onnxruntime.arm64.cntnr
```

### Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                               COMMAND       CREATED          STATUS          PORTS     NAMES
db3461bdfe0c   ampere.modelzoo.pytorch.intel.img   "/bin/bash"   21 seconds ago   Up 20 seconds             ampere.modelzoo.pytorch.intel.cntnr
```

### AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                             COMMAND       CREATED         STATUS         PORTS     NAMES
d690f53e7049   ampere.modelzoo.pytorch.amd.img   "/bin/bash"   6 seconds ago   Up 5 seconds             ampere.modelzoo.pytorch.amd.cntnr
```
