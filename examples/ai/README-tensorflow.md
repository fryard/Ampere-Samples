# 1. Build the base TensorFlow container image

## Arm64 Ampere
```
docker build -f Dockerfile.tensorflow.arm64 -t ampere.modelzoo.tensorflow.arm64.img .
```

# 2. Start a shell in the TensorFlow container

## Arm64 Ampere
```
docker run --hostname ampere-modelzoo-tensorflow-arm64-cntnr --privileged=true -it --rm -v $PWD/csv_files/arm64:/ampere/mzd/results/csv_files --name ampere.modelzoo.tensorflow.arm64.cntnr ampere.modelzoo.tensorflow.arm64.img
```

# 3. Run AI benchmarks

***note: you are executing inside the container shell***

## Arm64 Ampere
### Run resnet_50_v1.5 on 60 Ampere cores
`root@ampere-modelzoo-tensorflow-arm64-cntnr:/ampere/mzd#`

```
python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f tf -b 1 --debug -t 60
```

### Run resnet_50_v1.5 with AIO_IMPLICIT_FP16_TRANSFORM_FILTER on 60 Ampere cores
`root@ampere-modelzoo-tensorflow-arm64-cntnr:/ampere/mzd#`

```
AIO_IMPLICIT_FP16_TRANSFORM_FILTER=".*" python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f tf -b 1 --debug -t 60
```

# 4. Benchmark Results

## Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files/`

```
csv_files/
└── arm64
    └── tf@resnet_50_v1.5@fp32@ss.csv
```

# 5. Docker System Artifacts

## Images

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
ampere.modelzoo.tensorflow.arm64.img    latest    39ae40fff242   9 minutes ago    5.23GB
```

## Containers

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
d918223af844   ampere.modelzoo.tensorflow.arm64.img   "bash /.aio_init/gre…"   6 minutes ago   Up 6 minutes             ampere.modelzoo.tensorflow.arm64.cntnr
```
