# 1. Build the base PyTorch container image

## Arm64 Ampere
```
docker build -f Dockerfile.pytorch.arm64 -t ampere.modelzoo.pytorch.arm64.img .
```

## Intel
```
docker build -f Dockerfile.pytorch.intel -t ampere.modelzoo.pytorch.intel.img .
```

## AMD
```
docker build -f Dockerfile.pytorch.amd -t ampere.modelzoo.pytorch.amd.img .
```

# 2. Start a shell in the PyTorch container

## Arm64 Ampere
```
docker run --hostname ampere-modelzoo-pytorch-arm64-cntnr --privileged=true -it --rm -v $PWD/csv_files/arm64:/ampere/mzd/results/csv_files --name ampere.modelzoo.pytorch.arm64.cntnr ampere.modelzoo.pytorch.arm64.img
```

## Intel
```
docker run --hostname ampere-modelzoo-pytorch-intel-cntnr --privileged=true -it --rm -v $PWD/csv_files/intel:/ampere/mzd/results/csv_files --name ampere.modelzoo.pytorch.intel.cntnr ampere.modelzoo.pytorch.intel.img
```

## AMD
```
docker run --hostname ampere-modelzoo-pytorch-amd-cntnr --privileged=true -it --rm -v $PWD/csv_files/amd:/ampere/mzd/results/csv_files --name ampere.modelzoo.pytorch.amd.cntnr ampere.modelzoo.pytorch.amd.img
```

# 3. Run AI Benchmarks

***note: you are executing inside the container shell***

## Arm64 Ampere
### Run resnet_50_v1.5 on 60 Ampere cores
`root@ampere-modelzoo-pytorch-arm64-cntnr:/ampere/mzd#`

```
python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 60
```

### Run resnet_50_v1.5 with AIO_IMPLICIT_FP16_TRANSFORM_FILTER on 60 Ampere cores
`root@ampere-modelzoo-pytorch-arm64-cntnr:/ampere/mzd#`

```
AIO_IMPLICIT_FP16_TRANSFORM_FILTER=".*" python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 60
```

## Intel

### Run resnet_50_v1.5 on 6 Intel cores
`root@ampere-modelzoo-pytorch-intel-cntnr:/ampere/mzd#`

```
python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 6
```

### Run resnet_50_v1.5 with AIO_IMPLICIT_FP16_TRANSFORM_FILTER on 6 Intel cores
`root@ampere-modelzoo-pytorch-intel-cntnr:/ampere/mzd#`

```
AIO_IMPLICIT_FP16_TRANSFORM_FILTER=".*" python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 6
```

## AMD
### Run resnet_50_v1.5 on 6 AMD cores
`root@ampere-modelzoo-pytorch-amd-cntnr:/ampere/mzd#`

```
python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 6
```

### Run resnet_50_v1.5 with AIO_IMPLICIT_FP16_TRANSFORM_FILTER on 6 AMD cores
`root@ampere-modelzoo-pytorch-amd-cntnr:/ampere/mzd#`

```
AIO_IMPLICIT_FP16_TRANSFORM_FILTER=".*" python benchmark_ss.py -m resnet_50_v1.5 -p fp32 -f pytorch -b 1 --debug -t 6
```

# 4. Benchmark Results

## Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── arm64
    └── pytorch@resnet_50_v1.5@fp32@ss.csv
```

## Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── intel
    └── pytorch@resnet_50_v1.5@fp32@ss.csv
```

## AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── amd
    └── pytorch@resnet_50_v1.5@fp32@ss.csv
```

# 5. Docker System Artifacts

## Images

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                              TAG       IMAGE ID       CREATED          SIZE                                                                                   
ampere.modelzoo.pytorch.arm64.img       latest    e399f43ecd44   52 seconds ago   5.85GB
ampere.modelzoo.tensorflow.arm64.img    latest    39ae40fff242   2 minutes ago    5.23GB
ampere.modelzoo.onnxruntime.arm64.img   latest    89755e0443de   24 minutes ago   6.52GB
```

### Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                          TAG       IMAGE ID       CREATED          SIZE
ampere.modelzoo.pytorch.intel.img   latest    88a3e9e56012   16 seconds ago   7.17GB
```

### AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                        TAG       IMAGE ID       CREATED              SIZE
ampere.modelzoo.pytorch.amd.img   latest    341b8b165ca9   About a minute ago   13.8GB
```

## Containers

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                               COMMAND                  CREATED         STATUS         PORTS     NAMES
1a5c02251963   ampere.modelzoo.pytorch.arm64.img   "bash /.aio_init/gre…"   7 seconds ago   Up 6 seconds             ampere.modelzoo.pytorch.arm64.cntnr
```

### Intel
`root@intel-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                               COMMAND       CREATED          STATUS          PORTS     NAMES
db3461bdfe0c   ampere.modelzoo.pytorch.intel.img   "/bin/bash"   21 seconds ago   Up 20 seconds             ampere.modelzoo.pytorch.intel.cntnr
```

### AMD
`root@amd-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                             COMMAND       CREATED         STATUS         PORTS     NAMES
d690f53e7049   ampere.modelzoo.pytorch.amd.img   "/bin/bash"   6 seconds ago   Up 5 seconds             ampere.modelzoo.pytorch.amd.cntnr
```
