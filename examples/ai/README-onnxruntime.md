# 1. Build the base ONNX Runtime container image

## Arm64 Ampere
```
docker build -f Dockerfile.onnxruntime.arm64 -t ampere.modelzoo.onnxruntime.arm64.img .
```

# 2. Start a shell in the ONNX Runtime container

## Arm64 Ampere
```
docker run --hostname ampere-modelzoo-onnxruntime-arm64-cntnr --privileged=true -it --rm -v $PWD/csv_files/arm64:/ampere/mzd/results/csv_files --name ampere.modelzoo.onnxruntime.arm64.cntnr ampere.modelzoo.onnxruntime.arm64.img
```

# 3. Run AI benchmarks

***note: you are executing inside the container shell***

## Arm64 Ampere
### Run resnet_50_v1 on 60 Ampere cores
`root@ampere-modelzoo-onnxruntime-arm64-cntnr:/ampere/mzd#`

```
python benchmark_ss.py -m resnet_50_v1 -p fp32 -f ort -b 1 --debug -t 60
```

### Run resnet_50_v1 with AIO_IMPLICIT_FP16_TRANSFORM_FILTER on 60 Ampere cores
`root@ampere-modelzoo-onnxruntime-arm64-cntnr:/ampere/mzd#`

```
AIO_IMPLICIT_FP16_TRANSFORM_FILTER=".*" python benchmark_ss.py -m resnet_50_v1 -p fp32 -f ort -b 1 --debug -t 60
```

# 4. Benchmark Results

## Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# tree csv_files`

```
csv_files/
└── arm64
    └── ort@resnet_50_v1@fp32@ss.csv
```

# 5. Docker System Artifacts

## Images

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker images`

```
REPOSITORY                              TAG       IMAGE ID       CREATED         SIZE
ampere.modelzoo.onnxruntime.arm64.img   latest    89755e0443de   3 minutes ago   6.52GB
```

## Containers

### Arm64 Ampere
`root@ampere-machine:~/Source/Ampere-Samples/examples/ai# docker ps -a`

```
CONTAINER ID   IMAGE                                   COMMAND                  CREATED         STATUS         PORTS     NAMES
82c1f112f67b   ampere.modelzoo.onnxruntime.arm64.img   "bash /.aio_init/gre…"   7 seconds ago   Up 6 seconds             ampere.modelzoo.onnxruntime.arm64.cntnr
```
