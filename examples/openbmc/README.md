
# Ampere OpenBMC

# Prerequisites

1. Docker / Docker Compose

# Development Image and Container

## Build the base container and the example container

`docker-compose -f openbmc_img.yml build`

## Run the example container which builds the firmware (can be done without the build step above)

`docker-compose -f openbmc_img.yml -f openbmc_cntnr.yml up -d`

## Watch the container logs once the image is built

`docker logs --follow ampere.openbmc.cntnr`

## Stop and remove the container

`docker-compose -f openbmc_img.yml down`

*These steps will give you a container with the development environment*

# Debugging or ad-hoc usage of the container

## Get a shell into the *running container*

`docker exec -it ampere.openbmc.cntnr bash`

## Get a shell into the *base image*

`docker run -v $PWD:/root/share -it --rm --name ampere.openbmc.cntnr.$(date +%Y%m%d.%H%M%S) ampere.openbmc.img bash`

**See [build.sh](build.sh) for builds steps**

## Output

Look in the `/root/share` or the mapped local volume for the final binary produced by the container.

```
obmc-phosphor-image-mtjade_static_yyyymmdd.hhmmss.mtd
obmc-phosphor-image-mtjade_static_mtd_yyyymmdd.hhmmss.tar
```

*note: 20211103.230937 reflects the date/time when the build was completed*