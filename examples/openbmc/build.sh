#!/bin/bash
# SPDX-License-Identifier: BSD-2-Clause

# The build
echo "Building Ampere OpenBMC..."
SECONDS=0

# Keep container alive by tailing this
echo "Container started \"$(date +%Y%m%d.%H%M%S)\"" > ~/epoch_time.txt

# Get Ampere OpenBMC source
cd ~/ && git clone -b ampere https://github.com/ampere-openbmc/openbmc.git
echo "export PATH=$PATH:~/openbmc/poky/bitbake/bin" >> ~/.bashrc
source ~/.bashrc

# Set the openbmc source directory as our current working directory
# d32771e3 was the most current commit as of 2021/11/21. Pinning keeps it
# from breaking but has the consequence of not being the most current.
cd ~/openbmc && git checkout v1.16.100-ampere

# Configure the project for Ampere's Mt Jade reference platform for Altra and Altra Max
source setup mtjade

# Workaround for Bitbake complaint about running as root
touch conf/sanity.conf

# Build OpenBMC for Mt Jade
export LANG=en_US.UTF-8
bitbake obmc-phosphor-image

# Copy source to host
echo "Copying OpenBMC image to host"
cp tmp/work/mtjade-openbmc-linux-gnueabi/obmc-phosphor-image/1.0-r0/deploy-obmc-phosphor-image-image-complete/obmc-phosphor-image-mtjade.static.mtd \
   /root/share/obmc-phosphor-image-mtjade.static.mtd.$(date "+%Y%m%d.%H%M%S").mtd
cp tmp/work/mtjade-openbmc-linux-gnueabi/obmc-phosphor-image/1.0-r0/deploy-obmc-phosphor-image-image-complete/obmc-phosphor-image-mtjade.static.mtd.tar \
   /root/share/obmc-phosphor-image-mtjade.static.mtd.$(date "+%Y%m%d.%H%M%S").mtd.tar

echo "Done"
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

echo "Container running. Attach with <CTRL-C> then <docker exec -it ampere.openbmc.cntnr /bin/bash>"
echo "Container finshed \"$(date +%Y%m%d.%H%M%S)\"" >> ~/epoch_time.txt
tail -f ~/epoch_time.txt
