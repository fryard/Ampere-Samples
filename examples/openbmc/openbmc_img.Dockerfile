# SPDX-License-Identifier: BSD-2-Clause

ARG BUILDPLATFORM
# ubuntu:18.04 tagged to a specific time
FROM ubuntu:bionic-20210930
SHELL ["/bin/bash", "-c"]

WORKDIR /root

ARG BUILDPLATFORM
ENV BUILDPLATFORM=${BUILDPLATFORM}

# software-properties-common adds tzdata which will prompt for a time zone if noninteractive is not set
ARG DEBIAN_FRONTEND=noninteractive

# Set the language
RUN apt update && apt install -y \
    locales

RUN echo "LANG=en_US.UTF-8" > /etc/locale.conf
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen en_US.UTF-8

# Bitbake needs this and complains that Python can't change the language if missing
RUN update-locale "LANG=en_US.UTF-8"
RUN locale-gen --purge "en_US.UTF-8"
RUN dpkg-reconfigure --frontend noninteractive locales

RUN echo "export LANG=\"en_US.UTF-8\"" >> ~/.bashrc
RUN echo "export LANGUAGE=\"en_US:en\"" >> ~/.bashrc
RUN echo "export LC_ALL=\"en_US.UTF-8\"" >> ~/.bashrc
RUN source ~/.bashrc

# Install dependencies
RUN apt install -y \
    git \
    build-essential \
    libsdl1.2-dev \
    texinfo \
    gawk \
    chrpath \
    diffstat \
    cpio \
    file \
    zstd \
    liblz4-tool
    # note liblz4-tool changes to lz4 on newer Debian-esque distros (bitbake wants lz4c pzstd zstd)

# Install optional common utilities
RUN apt-get install -y \
      python \
      python-pip \
      vim \
      most \
      tree \
      curl \
      wget \
      unzip \
      jq \
      srecord \
      moreutils

# General proxy settings
RUN echo "# PROXY_URL=\"http://myproxy.mydomain.com:8080\"" >> /etc/profile
RUN echo "# export http_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export https_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export ftp_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export no_proxy=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for curl
RUN echo "# export HTTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export HTTPS_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export FTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export NO_PROXY=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for git
RUN echo "# [http]" > /root/.gitconfig
RUN echo "#         proxy = http://myproxy.mydomain.com:8080" >> /root/.gitconfig
RUN echo "# [https]" >> /root/.gitconfig
RUN echo "#         proxy = https://myproxy.mydomain.com:8080" >> /root/.gitconfig

# optional shell and editor defaults
RUN alias c=clear
RUN echo set background=dark > ~/.vimrc
RUN echo set tabstop=4 >> ~/.vimrc
RUN echo alias c=clear >> ~/.bashrc
RUN source ~/.bashrc
