# Enable X86Emulator module on Amepere Altra EDK2

X86EmulatorDxe module is a prebuilt binary to support x86 OptionROM loaded on ARM64 platform.

Upstream: https://github.com/tianocore/edk2-non-osi/tree/master/Emulator/X86EmulatorDxe

## Enable X86EmulatorDxe for Ampere Mt. Jade EDK2
1. Append the X86EmulatorDxe.inf into the [Jade.fdf](https://github.com/AmpereComputing/edk2-platforms/blob/ampere/Platform/Ampere/JadePkg/Jade.fdf) and [Jade.dsc](https://github.com/AmpereComputing/edk2-platforms/blob/ampere/Platform/Ampere/JadePkg/Jade.dsc) files ---> see the [patch](https://gitlab.com/-/snippets/2239063).

    `(cd edk2-platforms && wget -q -O - https://gitlab.com/uploads/-/system/personal_snippet/2239063/02e9f281bc5aae56f5a7fb09ee10010c/X86EmulatorDxe_Ampere_MtJade_EDK2_Enable.patch.gz | gunzip | git apply)`

    *note: the gzip step was needed due to how GitLab Snippets handle CRLF invalidating the textual patch*

<script src="https://gitlab.com/-/snippets/2239063/raw/main/X86EmulatorDxe_Ampere_MtJade_EDK2_Enable.patch"></script>

2. Recompile Mt. Jade’s EDK2 UEFI firmware with the new change.
3. EDK2 UEFI will load x86 OptionROM on PCIe card.
4. User can see driver health of PCIe card on UEFI Menu.

# Example: Tested with HBA BRCM 9400-8i SAS3408

![Option ROM Controller Properties image](docs/img/optrom-cntlrprop.png "Option ROM Controller Properties")

![Option ROM Device Manager image](docs/img/optrom-devmgr.png "Option ROM Device Manager")

![Option ROM Health Manager image](docs/img/optrom-drvheamgr.png "Option ROM Health Manager")

![Option ROM HBA Config image](docs/img/optrom-hbacfg.png "Option ROM HBA Config")