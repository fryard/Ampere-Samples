# SPDX-License-Identifier: BSD-2-Clause

ARG BUILDPLATFORM
# CentOS went EOL 2021/12/31 change to Rocky Linux
FROM rockylinux:8
SHELL ["/bin/bash", "-c"]
WORKDIR /root

ARG BUILDPLATFORM
ENV BUILDPLATFORM=${BUILDPLATFORM}

RUN yum install -y dnf epel-release.noarch wget curl rsync tree
RUN yum groupinstall -y "Development Tools"
RUN yum config-manager --set-enabled powertools
RUN dnf install -y zlib-devel ncurses-devel
RUN yum install -y ctags perl-XML-LibXML dtc libuuid-devel python2 python36 bc openssl-devel compat-openssl10 python3-pip lzma lzip
RUN if [ "$BUILDPLATFORM" = "amd64" ]; then yum install -y zlib-devel.i686 ncurses-devel.i686; fi

RUN if [ "$BUILDPLATFORM" = "amd64" ]; then (cd /usr/local/ && wget -q -O - https://go.dev/dl/go1.17.4.linux-amd64.tar.gz | tar -xzi); fi
RUN if [ "$BUILDPLATFORM" = "arm64" ]; then (cd /usr/local/ && wget -q -O - https://go.dev/dl/go1.17.4.linux-arm64.tar.gz | tar -xzi); fi

RUN echo "export GOPATH=/root/go" >> /root/.bashrc
ENV GOPATH="/root/go"
RUN mkdir $GOPATH
# https://github.com/u-root/u-root/commit/48e989bd2a069965a3c412b08048fafc9367428e#diff-678682767f2477de3e3c546746f8568b9a1942b2c647d32331d7e774b8ff8d9fL16
# This commit broke the Go u-boot module so you need the export GO111MODULE workaround
# Otherwise this step fails https://github.com/linuxboot/mainboards/blob/master/Makefile#L10
RUN echo "export GO111MODULE=off" >> /root/.bashrc
ENV GO111MODULE=off

RUN (cd /usr/bin && ln -sf python2 python)

RUN mkdir /root/build
WORKDIR /root/build

# Fix iasl
RUN wget https://acpica.org/sites/acpica/files/acpica-unix2-20201217.tar.gz -O - | tar -xz
RUN make -C acpica-unix2-20201217 -j $(nproc) HOST=_CYGWIN
RUN \cp acpica-unix2-20201217/generate/unix/bin/iasl /usr/bin/

# Install Ampere GCC compiler
RUN cd /root/build
# Note these are not .xz gzip formats but instead LZMA compression
RUN if [ "$BUILDPLATFORM" = "amd64" ]; then wget https://cdn.amperecomputing.com/tools/compilers/cross/8.3.0/ampere-8.3.0-20191025-dynamic-nosysroot-crosstools.tar.xz -O - | tar -xJ; \
elif [ "$BUILDPLATFORM" = "arm64" ]; then wget https://cdn.amperecomputing.com/tools/compilers/native/8.4.0/ampere-8.4.0-20200327-dynamic-nosysroot-nativetools.tar.xz -O - | tar -xJ; \
fi

RUN echo "export PATH=/root/build/ampere-8.4.0-20200327-dynamic-nosysroot/bin:/usr/local/go/bin:$GOPATH/bin:$PATH" >> /root/.bashrc
ENV PATH="/root/build/ampere-8.4.0-20200327-dynamic-nosysroot/bin:/usr/local/go/bin:$GOPATH/bin:${PATH}"

# General proxy settings
RUN echo "# PROXY_URL=\"http://myproxy.mydomain.com:8080\"" >> /etc/profile
RUN echo "# export http_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export https_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export ftp_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export no_proxy=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for curl
RUN echo "# export HTTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export HTTPS_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export FTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export NO_PROXY=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for git
RUN echo "# [http]" > /root/.gitconfig
RUN echo "#         proxy = http://myproxy.mydomain.com:8080" >> /root/.gitconfig
RUN echo "# [https]" >> /root/.gitconfig
RUN echo "#         proxy = https://myproxy.mydomain.com:8080" >> /root/.gitconfig

# optional shell and editor defaults
RUN alias c=clear
RUN echo set background=dark > /root/.vimrc
RUN echo set tabstop=4 >> /root/.vimrc
RUN echo alias c=clear >> /root/.bashrc
RUN source /root/.bashrc
