# Ampere EDK2 Example (LinuxBoot, Tianocore, Tianocore Capsule)

![Ampere EDK2 LinuxBoot image](docs/img/ampere-linuxboot-edk2.png "Ampere EDK2 LinuxBoot")

```
Linux version 5.7.0 (root@8f2119a9b21d) (gcc version 8.4.0 (Ampere Computing 8.4.0-20200327-dynamic-nosysroot), GNU ld (Ampere Computing 8.4.0-20200327-dynamic-nosysroot) 2.34) #1 SMP Sat Nov 6 23:18:00 PDT 2021
efi: EFI v2.70 by EDK II
efi: ACPI 2.0=0x81fffa30018 SMBIOS 3.0=0x81ffe6f0000 MEMATTR=0x81ffe68d698 MEMRESERVE=0x81fffff0018
```

### Generic Flashing Procedure

Reference `Upgrading EDK2 Firmware for Mt. Jade Platform` section in the Altra Open-Source EDK2 Firmware Porting Guide.

```
https://connect.amperecomputing.com/products
->High Performance 64-bit ARM Server Processors
-->Altra (Quicksilver)
--->Altra Software - Firmware
---->Porting Guide
----->Altra Open Source EDK2 Firmware Porting Guide
```

1. ssh to BMC console using the LinuxBoot binary generated from this project.

`amp_hostfw_update -c 1 -f jade_tianocore_atf_linuxboot_2.06.100.img -o 0x400000`

2. Check the OS console to verify the u-root shell.

ipmitool -I lanplus -U <username> -P <password> -H <BMC_IP> sol activate

3. Use `boot` command to select the OS you want to execute or manually mount the OS and bring it up.

*An alternate way to run the boot menu WIP.*


# Prerequisites

1. Docker / Docker Compose
2. Modify [.env](.env) to specify building on arm64 or amd64 *(default set to amd64)*
3. A downloaded Ampere Software Release Package (SRP)

```
    https://connect.amperecomputing.com/products
    ->High Performance 64-bit ARM Server Processors
    -->Altra Max (Mystique)
    --->Altra Max Software - Firmware
    ---->Software Packages
    ----->Ampere SRP aco2 r2.06.20220311 Binary Package
    ------>srp-ac02-fw-aptv-bin-r2.06.20220311.tar.xz
```

## Board Settings

Regardless whether it is a single socket or dual socket Mt Jade, use the regular board setting file without "1P".

```
altra_firmware_sdk/bin/board_settings/jade_board_setting_2.06.20220308.bin
altra_firmware_sdk/bin/board_settings/jade_board_setting_2.06.20220308.txt
```

"1P" image is only intended to force a two populated SoC into behaving similar to a single populated socket. The "1P" image will ignore the slave socket and be treated as a 1P system.
```
altra_firmware_sdk/bin/board_settings/jade_board_setting_1P_2.06.20220308.bin
altra_firmware_sdk/bin/board_settings/jade_board_setting_1P_2.06.20220308.txt
```

# Development Image and Container

## Build the base container and the example container

`docker-compose -f edk2_img.yml build`

*See [edk2_img.Dockerfile](edk2_img.Dockerfile) CentOS8 environment setup*

## Run the example container which builds the firmware (can be done without the build step above)

`docker-compose -f edk2_img.yml -f edk2_cntnr.yml up -d`

## Watch the container logs once the image is built

`docker logs --follow ampere.edk2.cntnr`

## Stop and remove the container

`docker-compose -f edk2_img.yml down`

*These steps will give you a container with the development environment*

# Debugging or ad-hoc usage of the container

## Get a shell into the *running container*

`docker exec -it ampere.edk2.cntnr bash`

## Get a shell into the *base image*

`docker run -v $PWD:/root/share -it --rm --name ampere.edk2.cntnr.$(date +%Y%m%d.%H%M%S) ampere.edk2.img bash`

**See [build.sh](build.sh) for EDK2 builds steps**

## Output

Look in the local volume for the final BUILDS-20220322.123631/ assests produced by the container.

```
BUILDS-20220322.123631
|-- [4.0K]  jade_tianocore_atf_2.06.100
|   |-- [ 76K]  CapsuleApp.efi
|   |-- [2.0K]  jade_board_setting.bin
|   |-- [647K]  jade_scp_2.06.100.cap
|   |-- [7.8M]  jade_tianocore_2.06.100.fd
|   |-- [ 13M]  jade_tianocore_atf_2.06.100.cap
|   `-- [9.8M]  jade_tianocore_atf_2.06.100.img
`-- [4.0K]  jade_tianocore_atf_linuxboot_2.06.100
    |-- [2.0K]  jade_board_setting.bin
    |-- [ 13M]  jade_tianocore_atf_linuxboot_2.06.100.img
    |-- [ 13M]  jade_tianocore_atf_linuxboot_2.06.100.img.raw
    `-- [ 11M]  jade_tianocore_linuxboot_2.06.100.fd
```

*note: 20211205.154116 reflects the date/time when the build assests were copied*
