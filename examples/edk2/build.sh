#!/bin/bash
# SPDX-License-Identifier: BSD-2-Clause

# The build
echo "Building EDK2 with LinuxBoot, Tianocore, Tianocore Capsule..."
SECONDS=0

# Keep container alive by tailing this
echo "Container started \"$(date +%Y%m%d.%H%M%S)\"" > /root/epoch_time.txt

cd /root/build

# Extract required Mt Jade board settings and ATF blobs from SRP archive
# --one-top-level to create the srp directory and put the extracted files there
# --strip-components to flatten the hierarchy
time tar -xf /root/share/srp-ac02-fw-aptv-bin-r2.06.20220311.tar.xz \
             srp-ac02-fw-aptv-bin-r2.06.20220311/altra_firmware_sdk/bin/atf/altra_atf_signed_2.06.20220308.slim \
             srp-ac02-fw-aptv-bin-r2.06.20220311/altra_firmware_sdk/bin/board_settings/jade_board_setting_2.06.20220308.bin \
             srp-ac02-fw-aptv-bin-r2.06.20220311/altra_firmware_sdk/bin/scp/altra_scp_signed_2.06.20220308.slim \
             --one-top-level=srp --strip-components=4

# Get Ampere EDK2 source
time git clone --recurse-submodules --branch ampere https://github.com/AmpereComputing/edk2.git
time git clone --branch ampere https://github.com/AmpereComputing/edk2-non-osi.git
time git clone --branch master https://github.com/AmpereComputing/edk2-ampere-tools.git
time git clone --recurse-submodules --branch v2.04.100-ampere https://github.com/AmpereComputing/edk2-platforms.git
time git clone --branch ampere-plan9-enablement https://github.com/dexter-ampere/mainboards.git

export TARGET_ARCH=arm64

if [ "$BUILDPLATFORM" = "amd64" ]; then
export X_COMPILER=/root/build/ampere-8.3.0-20191025-dynamic-nosysroot/bin/aarch64-ampere-linux-gnu-
elif [ "$BUILDPLATFORM" = "arm64" ]; then
export X_COMPILER=/root/build/ampere-8.4.0-20200327-dynamic-nosysroot/bin/aarch64-ampere-linux-gnu-
fi

time make -C mainboards/ampere/jade fetch flashkernel ARCH=$TARGET_ARCH CROSS_COMPILE=$X_COMPILER

# Build tianocore_img, tianocore_capsule, linuxboot_img
cd /root/build/edk2-ampere-tools

time make all \
    VER=2.06 \
    BUILD=100 \
    ATF_SLIM=../srp/altra_atf_signed_2.06.20220308.slim \
    BOARD_SETTING=../srp/jade_board_setting_2.06.20220308.bin \
    SCP_SLIM=../srp/altra_scp_signed_2.06.20220308.slim \
    LINUXBOOT_BIN=../mainboards/ampere/jade/flashkernel \
    ARCH=$TARGET_ARCH \
    CROSS_COMPILE=$X_COMPILER

# make tianocore_img \
#   VER=2.06 \
#   BUILD=100 \
#   ATF_SLIM=../srp/altra_atf_signed_2.06.20220308.slim \
#   BOARD_SETTING=../srp/jade_board_setting_2.06.20220308.bin \
#   ARCH=$TARGET_ARCH \
#   CROSS_COMPILE=$X_COMPILER

# make tianocore_capsule \
#   VER=2.06 \
#   BUILD=100 \
#   ATF_SLIM=../srp/altra_atf_signed_2.06.20220308.slim \
#   SCP_SLIM=../srp/altra_scp_signed_2.06.20220308.slim \
#   ARCH=$TARGET_ARCH \
#   CROSS_COMPILE=$X_COMPILER

# make linuxboot_img \
#   VER=2.06 \
#   BUILD=100 \
#   ATF_SLIM=../srp/altra_atf_signed_2.06.20220308.slim \
#   BOARD_SETTING=../srp/jade_board_setting_2.06.20220308.bin \
#   LINUXBOOT_BIN=mainboards/ampere/jade/flashkernel
#   ARCH=$TARGET_ARCH \
#   CROSS_COMPILE=$X_COMPILER

# Copy source to host
echo "Copying EDK2 image to host"
cp -R BUILDS /root/share/BUILDS-$(date "+%Y%m%d.%H%M%S")

echo "Done"
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

echo "Container running. Attach with <CTRL-C> then <docker exec -it ampere.edk2.cntnr /bin/bash>"
echo "Container finshed \"$(date +%Y%m%d.%H%M%S)\"" >> /root/epoch_time.txt
tail -f /root/epoch_time.txt
