# SPDX-License-Identifier: BSD-2-Clause

ARG BUILDPLATFORM
# Ubuntu 22.04 chose a static version instead of latest to prevent breakage
FROM ubuntu:jammy-20221130
SHELL ["/bin/bash", "-c"]
WORKDIR /root

ARG BUILDPLATFORM
ENV BUILDPLATFORM=${BUILDPLATFORM}

# Create build and download directories
RUN mkdir /root/build
# Tarball downloads
RUN mkdir /root/download
# Ampere tools compilers etc.
RUN mkdir /opt/amp

# software-properties-common adds tzdata which will prompt for a time zone if noninteractive is not set
ARG DEBIAN_FRONTEND=noninteractive

# 0. Get extra utilities
RUN apt update && apt install wget most moreutils jq tree unzip vim nano -y

# Needed for the Ampere 'fit create' tool
# RUN apt install python3-pip libparted-dev libparted python3-pkgconfig gdisk -y
RUN apt install gdisk libparted-dev pkg-config gcc python3-pip python3-dev -y

# Get compiler tarballs
WORKDIR /root/download
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu.tar.xz
RUN wget https://acpica.org/sites/acpica/files/acpica-unix-20220331.tar.gz

# 1. Install dependent packages
RUN apt install build-essential ssh git make default-jre gawk libaio1 numactl lsb-core environment-modules tcl uuid-dev python3 python3-numpy ipmitool tree gitk dos2unix samba sshpass expect screen bison flex m4 device-tree-compiler cmake gcc-aarch64-linux-gnu -y

# Needed for srp_3.3.1.1-tianocore_edk2_dev-202212100247/tools/cert_create/cert_create tool on Ubuntu 22.04
# Solves the following error
# cert_create: error while loading shared libraries: libcrypto.so.1.1: cannot open shared object file: No such file or directory
RUN wget https://mirror.umd.edu/ubuntu/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.16_amd64.deb
RUN dpkg -i libssl1.1_1.1.1f-1ubuntu2.16_amd64.deb

# Create a python symlink since the Makefiles don't specify python3
RUN ln -sf /usr/bin/python3 /usr/bin/python

# 2. Install GCC 11 to compile the EDK2and LinuxBoot.
RUN tar -xJf /root/download/gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu.tar.xz -C /opt/amp

# 3. Install IASL 20220331.
RUN tar xzf /root/download/acpica-unix-20220331.tar.gz --no-same-owner -C /opt/amp
WORKDIR /opt/amp/acpica-unix-20220331
RUN make HOST=_CYGWIN && make install

RUN if [ "$BUILDPLATFORM" = "amd64" ]; then (cd /usr/local/ && wget -q -O - https://go.dev/dl/go1.19.5.linux-amd64.tar.gz | tar -xzi); fi

RUN echo "export GOPATH=/root/go" >> /root/.bashrc
ENV GOPATH="/root/go"

RUN echo "export GOFLAGS=-modcacherw" >> /root/.bashrc
ENV GOFLAGS=-modcacherw
RUN mkdir $GOPATH

RUN echo "export GO111MODULE=auto" >> /root/.bashrc
ENV GO111MODULE=auto

WORKDIR /root/build

RUN echo "export PATH=/usr/local/go/bin:$GOPATH/bin:$PATH" >> /root/.bashrc
ENV PATH="/usr/local/go/bin:$GOPATH/bin:${PATH}"

# General proxy settings
RUN echo "# PROXY_URL=\"http://myproxy.mydomain.com:8080\"" >> /etc/profile
RUN echo "# export http_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export https_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export ftp_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export no_proxy=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for curl
RUN echo "# export HTTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export HTTPS_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export FTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export NO_PROXY=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for git
RUN echo "# [http]" > /root/.gitconfig
RUN echo "#         proxy = http://myproxy.mydomain.com:8080" >> /root/.gitconfig
RUN echo "# [https]" >> /root/.gitconfig
RUN echo "#         proxy = https://myproxy.mydomain.com:8080" >> /root/.gitconfig

# optional shell and editor defaults
RUN alias c=clear
RUN echo set background=dark > /root/.vimrc
RUN echo set tabstop=4 >> /root/.vimrc
RUN echo alias c=clear >> /root/.bashrc
RUN source /root/.bashrc
