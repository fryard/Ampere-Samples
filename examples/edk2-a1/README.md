# AmpereOne EDK2 Example (LinuxBoot, Tianocore, Tianocore Capsule, FIT)

# Prerequisites

1. Docker / Docker Compose or Podman / Podman Compose
2. Modify [.env](.env) to specify building on amd64 *(amd64 only at this time)*
3. A downloaded Ampere EDK2 Software Release Source Package (SRP) tarball put in this directory.

```
https://solutions.amperecomputing.com/customer-connect/products
->AmpereOne
-->AmpereOne Software - Firmware
--->Software Packages
---->Ampere SRP 3.3.1.1 Tianocore EDK2 Development package
----->srp_3.3.1.1-tianocore_edk2_dev-202212100247.tar.xz

```

# Podman / Docker Notes

## Build machine

`lsb_release -a`

```
Distributor ID: Ubuntu
Description:    Ubuntu 22.04.1 LTS
Release:        22.04
Codename:       jammy
```

## Podman registries used in on build machine.

`cat /etc/containers/registries.conf`

```
[registries.search]
registries = ["registry.access.redhat.com", "quay.io", "docker.io"]
```

## Compatibility between Docker/Docker Compose and Podman/Podman Compose

These container were adopted from a previous pure-Docker container definition. As such they should work with Docker and were only tested with Podman. Since Podman defaults to OSI containers there is only one syntactical change needed to run with Podman.

*--podman-build-args='--format docker'*

# Development Image and Container

## Build the base container and the example container

`podman-compose -f edk2_a1_img.yml --podman-build-args='--format docker' build`

*See [edk2_a1_img.Dockerfile](edk2_a1_img.Dockerfile) OS environment setup*

## Run the example container which builds the firmware

`podman-compose -f edk2_a1_img.yml -f edk2_a1_cntnr.yml up -d`

## Watch the container logs once the image is built

`podman logs --follow ampere.edk2.a1.cntnr`

## Stop and remove the container

`podman-compose -f edk2_a1_img.yml down`

*These steps will give you a container with the development environment*

# Debugging or ad-hoc usage of the container

## Get a shell into the *running container*

`podman exec -it ampere.edk2.a1.cntnr bash`

## Get a shell into the *base image*

`podman run -v $PWD:/root/share -it --rm --name ampere.edk2.a1.cntnr.$(date +%Y%m%d.%H%M%S) ampere.edk2.a1.img bash`

**See [build.sh](build.sh) for EDK2 builds steps**

## Output

Look in the local volume for the final BUILDS-20220322.123631/ assests produced by the container.

```
BUILDS-20230119.102937
|-- ampereOneCrb_tianocore_debug_0.00.00001001.fd
|-- ampereOneCrb_tianocore_release_0.00.00001001.fd
|-- ampereOneCrb_tianocore_release_linuxboot_0.00.00001001.fd
|-- ampere_stmm_debug.pkg
|-- ampere_stmm_release.pkg
|-- BL32_AP_MM_debug.fd
|-- BL32_AP_MM_release.fd
|-- BL33_AMPEREONECRB_UEFI_DEBUG.fd
|-- BL33_AMPEREONECRB_UEFI_RELEASE.fd
|-- BL33_AMPEREONECRB_UEFI_release_linuxboot.fd
|-- fip_debug.bin
|-- fip_release.bin
|-- manifest.bin
|-- nvparam_eeprom_build
|   |-- ac03
|   |   `-- mt_mitchell
|   |       |-- nvpb
|   |       |   `-- nvpberly.nvp
|   |       |-- nvpd
|   |       |   |-- nvpd2pl0.nvp
|   |       |   |-- nvpdboot.nvp
|   |       |   |-- nvpdddr0.nvp
|   |       |   |-- nvpdddr1.nvp
|   |       |   |-- nvpdoem.nvp
|   |       |   |-- nvpdpci0.nvp
|   |       |   |-- nvpdpci1.nvp
|   |       |   |-- nvpdpwrm.nvp
|   |       |   |-- nvpdras.nvp
|   |       |   `-- nvpdtpm0.nvp
|   |       `-- nvps
|   |           |-- nvps2pl0.nvp
|   |           |-- nvpsboot.nvp
|   |           |-- nvpsddr0.nvp
|   |           |-- nvpsddr1.nvp
|   |           |-- nvpslio0.nvp
|   |           |-- nvpslio1.nvp
|   |           |-- nvpsoem.nvp
|   |           |-- nvpspci0.nvp
|   |           |-- nvpspci1.nvp
|   |           |-- nvpspwrm.nvp
|   |           |-- nvpsras.nvp
|   |           |-- nvpsspmc.nvp
|   |           `-- nvpstpm0.nvp
|   `-- EEPROM.bin
|-- sp_extra_cert_debug
|-- sp_extra_cert_release
`-- spi-nor.img
```

*note: 20211205.154116 reflects the date/time when the build assests were copied*
