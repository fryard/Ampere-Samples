#!/bin/bash
# SPDX-License-Identifier: BSD-2-Clause

# The build
echo "Building EDK2 with LinuxBoot, Tianocore, Tianocore Capsule..."
SECONDS=0
export TARGET_ARCH=arm64

export SRP_BUILD_DIR=/root/build/srp
export SRP_NAME_VER=srp_3.3.1.1-tianocore_edk2_dev-202212100247

# Keep container alive by tailing this
export STARTDATETIMESTAMP=$(date +%Y%m%d.%H%M%S)
echo "Container started $STARTDATETIMESTAMP" > /root/epoch_time.txt
export OUTPUT_DIR=/root/share/BUILDS-$STARTDATETIMESTAMP
mkdir $OUTPUT_DIR

cd /root/build

echo "Extract required edk and edk2-platforms zips from the tarball"
# --one-top-level to create the srp directory and put the extracted files there
# --strip-components to flatten the hierarchy
time tar -xf /root/share/srp_3_3_1_1_tianocore_edk2_dev_202212100247_tar_4d33837a45.xz \
             $SRP_NAME_VER/fw/src/edk2-platforms/edk2-platforms.zip \
             $SRP_NAME_VER/fw/src/edk2/edk2.zip \
             --one-top-level=srp --strip-components=4

echo "Extract required linuxboot archive from the tarball"
# --one-top-level to create the srp directory and put the extracted files there
# --strip-components to flatten the hierarchy
time tar -xf /root/share/srp_3_3_1_1_tianocore_edk2_dev_202212100247_tar_4d33837a45.xz \
             $SRP_NAME_VER/fw/src/linuxboot/linuxboot.tar.gz \
             --one-top-level=srp --strip-components=4

echo "Uncompressing linuxboot, edk2-platforms, and edk2 to $SRP_BUILD_DIR"
cd $SRP_BUILD_DIR
unzip edk2-platforms.zip
unzip edk2.zip
tar -xzf linuxboot.tar.gz
# Needed to build NVPARAM and EEPROM
tar -xf /root/share/srp_3_3_1_1_tianocore_edk2_dev_202212100247_tar_4d33837a45.xz

# GCC 11.1 has an incompatibility that needs -Wno-vla-parameter to build brotli without errors
# https://github.com/patmagauran/i915ovmfPkg/issues/21
# Fixup the makefile 
sed -i 's/\-fno\-delete\-null\-pointer\-checks \-Wall \-Werror \\/\-fno\-delete\-null\-pointer\-checks \-Wall \-Werror \-Wno-vla-parameter \\/g' $SRP_BUILD_DIR/edk2/BaseTools/Source/C/Makefiles/header.makefile

# Build edk2-platforms
cd $SRP_BUILD_DIR/edk2-platforms/Platform/Ampere/AmpereOneCrbPkg

echo "Building tianocore_release"
make tianocore_release

echo "Building tianocore_debug"
make tianocore_debug

# Build linuxboot
cd $SRP_BUILD_DIR/linuxboot-srp-3.1.1.0-rc2

echo "Patching the linuxboot Makefile to work with golang >=1.18"
cp /root/share/Makefile.linuxboot $SRP_BUILD_DIR/linuxboot-srp-3.1.1.0-rc2/mainboards/ampere/mitchell/Makefile
echo "Building linuxboot flash kernel"
make -C ./mainboards/ampere/mitchell/ fetch flashkernel

echo "Building tianocore_linuxboot_release"
# 3. copy the generated flashkernel to edk2-platforms
cp $SRP_BUILD_DIR/linuxboot-srp-3.1.1.0-rc2/mainboards/ampere/mitchell/flashkernel $SRP_BUILD_DIR/edk2-platforms/Platform/Ampere/LinuxBootPkg/Aarch64
cd $SRP_BUILD_DIR/edk2-platforms/Platform/Ampere/AmpereOneCrbPkg
make tianocore_linuxboot_release

echo "Building tianocore_mm_release"
# 3.3 Building EDK2-MM
cd $SRP_BUILD_DIR/edk2-platforms/Platform/Ampere/AmpereOneCrbPkg
make tianocore_mm_release
echo "Building tianocore_mm_debug"
make tianocore_mm_debug

echo "Building NVPARAM and EEPROM"
cd $SRP_BUILD_DIR/$SRP_NAME_VER/fw/src/sys-config/nvparam
chmod +x nvparam_checksum8.py
make

echo "Building FIP images"
# make sptool executable
chmod +x $SRP_BUILD_DIR/$SRP_NAME_VER/tools/sptool/sptool
chmod +x $SRP_BUILD_DIR/$SRP_NAME_VER/tools/cert_create/cert_create
chmod +x $SRP_BUILD_DIR/$SRP_NAME_VER/tools/fiptool/fiptool

echo "Building Secure Partition blob with sptool"
# ampere_stmm_release blob
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/sptool/sptool -i $SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/RELEASE_GCC5/FV/BL32_AP_MM.fd:$SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/RELEASE_GCC5/AARCH64/Platform/Ampere/AmpereOneCrbPkg/DeviceTree/MitchellDeviceTree/OUTPUT/edk2_mm.dtb -o ampere_stmm_release.pkg

# ampere_stmm_release blob
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/sptool/sptool -i $SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/DEBUG_GCC5/FV/BL32_AP_MM.fd:$SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/DEBUG_GCC5/AARCH64/Platform/Ampere/AmpereOneCrbPkg/DeviceTree/MitchellDeviceTree/OUTPUT/edk2_mm.dtb -o ampere_stmm_debug.pkg

echo "Building extra certs with cert_create"
# sp_extra_cert_release
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/cert_create/cert_create --new-keys --tfw-nvctr 31 --sp-pkg1 ampere_stmm_release.pkg --sip-sp-cert sp_extra_cert_release

# sp_extra_cert_debug
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/cert_create/cert_create --new-keys --tfw-nvctr 31 --sp-pkg1 ampere_stmm_debug.pkg --sip-sp-cert sp_extra_cert_debug

echo "Building fip image with fiptool"
# fip_release.bin
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/fiptool/fiptool --verbose update --blob uuid=b4b5671e-4a90-4fe1-b81f-fb13dae1dacb,file=ampere_stmm_release.pkg --sip-sp-cert sp_extra_cert_release fip_release.bin

# fip_debug.bin
$SRP_BUILD_DIR/$SRP_NAME_VER/tools/fiptool/fiptool --verbose update --blob uuid=b4b5671e-4a90-4fe1-b81f-fb13dae1dacb,file=ampere_stmm_debug.pkg --sip-sp-cert sp_extra_cert_debug fip_debug.bin

# Install the fit tool from the SRP archive
pip install $SRP_BUILD_DIR/$SRP_NAME_VER/tools/fit/ampere_fit-0.22.2-py3-none-any.whl

# Run the fit tool
cd $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/components
fit create --config-file $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/config/spinor.json --output-file spi-nor.img

# Install the hpm_generator from the SRP archive
pip install $SRP_BUILD_DIR/$SRP_NAME_VER/tools/hpm-generator/hpm_generator-1.0.6-py3-none-any.whl

# This section is needed to get hostfw_image.json
# Uncomopress additional HPM creator tools included in the SRP for x86
cd $SRP_BUILD_DIR/$SRP_NAME_VER/tools/hpm-generator
tar xf hpm-generator-bin-1.0.6-x86_64.tar.xz
cd hpm-generator
# Make tools executable
chmod +x hpm-creator hpm-gen-config
# Generates hpm_conf.json (-c bsd is for EEPROM in config and hostfw is for spi-nor)
./hpm-gen-config -v 3.2.1 -c hostfw -i $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/components/spi-nor.img -p mitchell
# Generates hostfw_image.json (creates spinor_3.2.1.hpm)
./hpm-creator -i ./hpm_conf.json

# Currently in the porting guide but incomplete and needs to do the steps above instead with hpm-creator and hpm-gen-config
# 5.4 Generate the final HPM Image
# python3 -m hpm_generator.config_gen \
#         --version 0.0.0 \
#         --component hostfw \
#         --input ./spi-nor.img \
#         # --input $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/images/spinor.img \
#         --platform mitchell \
#         --output ./ \
#         --filename hostfw_image.json \
#         --hpm-name spinor

# # # 5.5 Generate an HPM Configuration File
# python3 -m hpm_generator.amp_hpm_creator \
#         --component hostfw \
#         --input $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/components/hostfw_image.json \
#         --outdir ./

echo "Copying FD images to host"
# tianocore release
cp $SRP_BUILD_DIR/bin/edk2/ampereOneCrb_tianocore_release_0.00.00001001.fd $OUTPUT_DIR
cp $SRP_BUILD_DIR/edk2-platforms/Build/AmpereOneCrb/RELEASE_GCC5/FV/BL33_AMPEREONECRB_UEFI.fd $OUTPUT_DIR/BL33_AMPEREONECRB_UEFI_RELEASE.fd

# tianocore debug
cp $SRP_BUILD_DIR/bin/edk2/ampereOneCrb_tianocore_debug_0.00.00001001.fd $OUTPUT_DIR
cp $SRP_BUILD_DIR/edk2-platforms/Build/AmpereOneCrb/DEBUG_GCC5/FV/BL33_AMPEREONECRB_UEFI.fd $OUTPUT_DIR/BL33_AMPEREONECRB_UEFI_DEBUG.fd

# linuxboot release
cp $SRP_BUILD_DIR/bin/edk2/ampereOneCrb_tianocore_release_0.00.00001001.fd $OUTPUT_DIR/ampereOneCrb_tianocore_release_linuxboot_0.00.00001001.fd
cp $SRP_BUILD_DIR/edk2-platforms/Build/AmpereOneCrb/RELEASE_GCC5/FV/BL33_AMPEREONECRB_UEFI.fd $OUTPUT_DIR/BL33_AMPEREONECRB_UEFI_release_linuxboot.fd

# EDK2-MM release
cp $SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/RELEASE_GCC5/FV/BL32_AP_MM.fd $OUTPUT_DIR/BL32_AP_MM_release.fd

# EDK2-MM release
cp $SRP_BUILD_DIR/edk2-platforms/Build/AmpereStandaloneMm/DEBUG_GCC5/FV/BL32_AP_MM.fd $OUTPUT_DIR/BL32_AP_MM_debug.fd

# NVPARAM binaries
cp -R $SRP_BUILD_DIR/$SRP_NAME_VER/fw/src/sys-config/nvparam/build $OUTPUT_DIR/nvparam_eeprom_build

# EEPROM binaries
cp $SRP_BUILD_DIR/$SRP_NAME_VER/fw/src/sys-config/nvparam/build/ac03/mt_mitchell/nvpb/nvpberly.nvp $OUTPUT_DIR/nvparam_eeprom_build/EEPROM.bin

# certs and fip images
cd $SRP_BUILD_DIR/$SRP_NAME_VER/fw/src/sys-config/nvparam
cp ampere_stmm_release.pkg ampere_stmm_debug.pkg sp_extra_cert_release sp_extra_cert_debug fip_release.bin fip_debug.bin $OUTPUT_DIR/

# SPI-NOR image and manifest - note this uses default blobs so the last build that
# occurred from above will be used. In most cases we do release then debug so it will be debug
cd $SRP_BUILD_DIR/$SRP_NAME_VER/fw/bin/spinor/components
cp manifest.bin spi-nor.img $OUTPUT_DIR

cd $SRP_BUILD_DIR/$SRP_NAME_VER/tools/hpm-generator/hpm-generator
cp spinor_3.2.1.hpm hpm_conf.json $OUTPUT_DIR

# Get back to the SRP directory
cd $SRP_BUILD_DIR

echo "Done"
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

echo "Container running. Attach with <CTRL-C> then <docker exec -it ampere.edk2.a1.cntnr /bin/bash>"
echo "Container finshed \"$(date +%Y%m%d.%H%M%S)\"" >> /root/epoch_time.txt
tail -f /root/epoch_time.txt
