#!/bin/bash
# SPDX-License-Identifier: BSD-2-Clause

# The build
echo "Building Flashrom project"
SECONDS=0

# Keep container alive by tailing this
echo "Container started \"$(date +%Y%m%d.%H%M%S)\"" > /root/epoch_time.txt

cd /root/build

# Get Ampere Flashrom source that includes ati_spi support for
# flashing AMD graphics cards
time git clone --branch ati https://github.com/ardbiesheuvel/flashrom
cd flashrom && git describe --always

make CONFIG_ENABLE_LIBUSB0_PROGRAMMERS=no CONFIG_ENABLE_LIBUSB1_PROGRAMMERS=no
make install CONFIG_ENABLE_LIBUSB0_PROGRAMMERS=no CONFIG_ENABLE_LIBUSB1_PROGRAMMERS=no

time git clone --recurse-submodules https://github.com/tianocore/edk2
cd edk2 && git describe --always && make -C BaseTools

mkdir -p /root/build/amd_option_roms
cd /root/build/amd_option_roms

# Run flashrom to get extract the ROM from the AMD Radeon HD5450
flashrom -pati_spi -r gpu_hd5450-orig.rom

# Run the EDK2 EfiRom tool to dump the OEM ROM text
EfiRom -d gpu_hd5450-orig.rom > gpu_hd5450-orig.rom.txt

# make a copy of the OEM rom to convert to aarch64/arm64
cp gpu_hd5450-orig.rom  radeon-5450-arm64.rom

# Download the AMD GOP driver
# https://www.amd.com/en/support/kb/release-notes/rn-aar
wget https://download.amd.com/drivers/Arm64Gop_1_68.efi

# Convert BIOS GOP Driver Arm64Gop_1_68.efi to .rom image
EfiRom -f 0x1002 -i 0x68F9 -o Arm64Gop_1_68.rom -ec Arm64Gop_1_68.efi

# Run the EDK2 EfiRom tool to dump the new Arm64 ROM text
EfiRom -d Arm64Gop_1_68.rom > Arm64Gop_1_68.rom.txt

# Read Arm64Gop_1_68.rom and write to image-2 offset of radeon-5450-arm64.rom
#  Choose the dd block size(bs) such that bs multiplied with seek offset in dd command is equal to image-2 offset
#  Example: if image-2 offset is 0x10000 the seek=256 and bs=256 (256 x 256 = 0x10000)
dd if=Arm64Gop_1_68.rom of=radeon-5450-arm64.rom seek=256 bs=256 conv=notrunc

# Dump the radeon-5450-arm64.rom to check if everything looks good.
#  Check for AA64 header in in EFI ROM Header
EfiRom -d radeon-5450-arm64.rom > radeon-5450-arm64.rom.txt

# Flash the radeon-5450-arm64.rom to GPU card
echo "Flash the radeon-5450-arm64.rom to GPU card"
echo "----> flashrom -pati_spi -w radeon-5450-arm64.rom"
echo "REBOOT SYSTEM AFTER RUNNING THE FLASH COMMAND ABOVE"

# Copy option ROM images to host
echo "Copying AMD Option ROM images to host"
cp -R /root/build/amd_option_roms /root/share/AMD_OPTION_ROMS-$(date "+%Y%m%d.%H%M%S")

echo "Done"
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

echo "Container running. Attach with <CTRL-C> then <docker exec -it ampere.flashrom.cntnr /bin/bash>"
echo "Container finshed \"$(date +%Y%m%d.%H%M%S)\"" >> /root/epoch_time.txt
tail -f /root/epoch_time.txt
