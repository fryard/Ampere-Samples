# SPDX-License-Identifier: BSD-2-Clause

FROM centos:7
SHELL ["/bin/bash", "-c"]
WORKDIR /root

RUN yum install -y dnf epel-release.noarch wget curl rsync tree most moreutils
RUN yum groupinstall -y "Development Tools"
RUN yum -y install pciutils-devel uuid libuuid-devel

RUN mkdir /root/build
WORKDIR /root/build

# /usr/local/sbin is where flashrom lands with make install
RUN echo "export PATH=/usr/local/sbin:/root/build/flashrom/edk2/BaseTools/Source/C/bin:$PATH" >> /root/.bashrc
ENV PATH="/usr/local/sbin:/root/build/flashrom/edk2/BaseTools/Source/C/bin:${PATH}"

# General proxy settings
RUN echo "# PROXY_URL=\"http://myproxy.mydomain.com:8080\"" >> /etc/profile
RUN echo "# export http_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export https_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export ftp_proxy=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export no_proxy=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for curl
RUN echo "# export HTTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export HTTPS_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export FTP_PROXY=\"$PROXY_URL\"" >> /etc/profile
RUN echo "# export NO_PROXY=\"127.0.0.1,localhost\"" >> /etc/profile

# Proxy settings for git
RUN echo "# [http]" > /root/.gitconfig
RUN echo "#         proxy = http://myproxy.mydomain.com:8080" >> /root/.gitconfig
RUN echo "# [https]" >> /root/.gitconfig
RUN echo "#         proxy = https://myproxy.mydomain.com:8080" >> /root/.gitconfig

# optional shell and editor defaults
RUN alias c=clear
RUN echo set background=dark > /root/.vimrc
RUN echo set tabstop=4 >> /root/.vimrc
RUN echo alias c=clear >> /root/.bashrc
RUN source /root/.bashrc
