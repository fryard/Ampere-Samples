# Ampere Flashrom Example

This container will run in *privileged* mode and read the stock x86 ROM from the VisionTek Radeon 5450 card and output a final Arm64 ROM. The last step is intentionally commented out to run under manual supervision in order to not corrupt anything.

This container should be harmless and only produce errors if the assumed hardware is not present.

### Elements produced by this container

- Arm64Gop_1_68.efi *downloaded from AMD*
- Arm64Gop_1_68.rom
- Arm64Gop_1_68.rom.txt
- gpu_hd5450-orig.rom
- gpu_hd5450-orig.rom.txt
- radeon-5450-arm64.rom
- radeon-5450-arm64.rom.txt
- flashrom binary executable with PCIe SPI programming for ATI
- EfiRom binary executable
- AMD_OPTION_ROMS-20220208.132511 folder on the host system with EFI/ROM/TXT files above

Once the container is built you should see the following text from `docker logs -f ampere.flashrom.cntnr` that indicates it is complete.

```
Flash the radeon-5450-arm64.rom to GPU card
----> flashrom -pati_spi -w radeon-5450-arm64.rom
REBOOT SYSTEM AFTER RUNNING THE FLASH COMMAND ABOVE
Copying AMD Option ROM images to host
Done
0 minutes and 3 seconds elapsed.
Container running. Attach with <CTRL-C> then <docker exec -it ampere.flashrom.cntnr /bin/bash>
Container started "20220208.140554"
Container finshed "20220208.140557"
```

Start a shell into the *running container*

`docker exec -it ampere.flashrom.cntnr bash`

`[root@6789890d2091 build]# ls -la /root/build/amd_option_roms`

```
total 868
drwxr-xr-x 2 root root   4096 Feb  8 14:05 .
drwxr-xr-x 1 root root   4096 Feb  8 13:25 ..
-rw-r--r-- 1 root root 136896 May 13  2019 Arm64Gop_1_68.efi
-rw-r--r-- 1 root root  64000 Feb  8 14:05 Arm64Gop_1_68.rom
-rw-r--r-- 1 root root    875 Feb  8 14:05 Arm64Gop_1_68.rom.txt
-rw-r--r-- 1 root root 262144 Feb  8 13:25 gpu_hd5450-orig.rom
-rw-r--r-- 1 root root   1486 Feb  8 14:05 gpu_hd5450-orig.rom.txt
-rw-r--r-- 1 root root 262144 Feb  8 14:05 radeon-5450-arm64.rom
-rw-r--r-- 1 root root   1483 Feb  8 14:05 radeon-5450-arm64.rom.txt
```

Run the `flashrom` command listed above to program the `radeon-5450-arm64.rom` on the card.

## Helpful Links

### Drivers and HowTo

[WorkOfArd: AArch64 option ROMs for AMD GPUs](https://www.workofard.com/2020/12/aarch64-option-roms-for-amd-gpus/)

[YouTube: Flashing UEFI Graphics GOP Driver on AMD Radeon GPUs for Aarch64 Systems](https://www.youtube.com/watch?v=3cYJdw3Y9_Y)

[AMD: GOP Driver](https://www.amd.com/en/support/kb/release-notes/rn-aar)

### Hardware Info

    *The VGA port seems to be the only one the AMD GOP driver initializes on Arm64

[Amazon: VisionTek Radeon 5450 used in this example](https://www.amazon.com/gp/product/B01CN1F1ZM/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&th=1)

[Lenovo: Legion T5-26IOB6 Desktop (Lenovo) - Type 90RS Model 90RS001MUS ](https://pcsupport.lenovo.com/us/en/products/desktops-and-all-in-ones/legion-series/legion-t5-26iob6/90rs/90rs001mus)

[VisionTek: Radeon 5450](https://www.visiontek.com/products/visiontek-radeon%E2%84%A2-5450-2gb-ddr3-dvi-i-hdmi-vga)

[DeviceHunt: PCIe Device Information Vendor 0x1002 Device 0x68F9](https://devicehunt.com/view/type/pci/vendor/1002/device/68F9)

[TechPowerUp: ATI Radeon HD 5450](https://www.techpowerup.com/gpu-specs/radeon-hd-5450.c503)


## Flashing the AMD Radeon HD 5450 with an Arm64 ROM

`flashrom -pati_spi -w radeon-5450-arm64.rom`

```
flashrom  on Linux 5.10.0-11-amd64 (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
Detected 1002:68f9@01:00.0 "Advanced Micro Devices, Inc. [AMD/ATI] Cedar [Radeon HD 5000/6000/7350/8350 Series]"
===
This PCI device is UNTESTED. Please report the 'flashrom -p xxxx' output
to flashrom@flashrom.org if it works for you. Please add the name of your
PCI device to the subject. Thank you for your help!
===
===
SFDP has autodetected a flash chip which is not natively supported by flashrom yet.
All standard operations (read, verify, erase and write) should work, but to support all possible features we need to add them manually.
You can help us by mailing us the output of the following command to flashrom@flashrom.org:
'flashrom -VV [plus the -p/--programmer parameter]'
Thanks for your help!
===
Found Unknown flash chip "SFDP-capable chip" (256 kB, SPI) on ati_spi.
Reading old flash chip contents... done.
Erasing and writing flash chip... Erase/write done.
Verifying flash... VERIFIED.
```

# Assumptions and Prerequisites

1. Docker / Docker Compose
2. x86 Intel PC (needed for SPI PCIe programming)
3. A VisionTek Radeon 5450 card installed

# Development Image and Container

## Containers and Images Produced

```
username@legion-workstation:~$ docker ps -a
CONTAINER ID   IMAGE                 COMMAND                  CREATED          STATUS         PORTS     NAMES
6789890d2091   ampere.flashrom.img   "/root/share/build.sh"   42 minutes ago   Up 2 seconds             ampere.flashrom.cntnr

username@legion-workstation:~$ docker images
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
ampere.flashrom.img   latest    61b824e733bb   55 minutes ago   1.13GB
centos                7         eeb6ee3f44bd   4 months ago     204MB
```

## Build the base container and the example container

`docker-compose -f flashrom_img.yml build`

*See [flashrom_img.Dockerfile](flashrom_img.Dockerfile) CentOS7 environment setup*

## Run the example container which builds the firmware (can be done without the build step above)

`docker-compose -f flashrom_img.yml -f flashrom_cntnr.yml up -d`

## Watch the container logs once the image is built

`docker logs --follow ampere.flashrom.cntnr`

## Stop and remove the container

`docker-compose -f flashrom_img.yml down`

*These steps will give you a container with the development environment*

# Debugging or ad-hoc usage of the container

## Get a shell into the *running container*

`docker exec -it ampere.flashrom.cntnr bash`

## Get a shell into the *base image*

`docker run -v $PWD:/root/share -it --rm --name ampere.flashrom.cntnr.$(date +%Y%m%d.%H%M%S) ampere.flashrom.img bash`

**See [build.sh](build.sh) for Flashrom builds steps**

# Extended EfiRom Dump Data

## gpu_hd5450-orig.rom.txt

```
Image 1 -- Offset 0x0
  ROM header contents
    Signature              0xAA55
    PCIR offset            0x01EC
    Signature               PCIR
    Vendor ID               0x1002
    Device ID               0x68F9
    Length                  0x0018
    Revision                0x0000
    DeviceListOffset        0x00
    Class Code              0x030000
    Image size              0x10000
    Code revision:          0x0C14
    MaxRuntimeImageLength   0x00
    ConfigUtilityCodeHeaderOffset 0x5441
    DMTFCLPEntryPointOffset 0x2049
    Indicator               0x00
    Code type               0x00
Image 2 -- Offset 0x10000
  ROM header contents
    Signature              0xAA55
    PCIR offset            0x001C
    Signature               PCIR
    Vendor ID               0x1002
    Device ID               0x68F9
    Length                  0x0018
    Revision                0x0000
    DeviceListOffset        0x00
    Class Code              0x030000
    Image size              0xE400
    Code revision:          0x0000
    MaxRuntimeImageLength   0x00
    ConfigUtilityCodeHeaderOffset 0x4F47
    DMTFCLPEntryPointOffset 0x2050
    Indicator               0x80   (last image)
    Code type               0x03   (EFI image)
  EFI ROM header contents
    EFI Signature          0x0EF1
    Compression Type       0x0001 (compressed)
    Machine type           0x8664 (X64)
    Subsystem              0x000B (EFI boot service driver)
    EFI image offset       0x0058 (@0x10058)
```

## Arm64Gop_1_68.rom.txt

```
Image 1 -- Offset 0x0
  ROM header contents
    Signature              0xAA55
    PCIR offset            0x001C
    Signature               PCIR
    Vendor ID               0x1002
    Device ID               0x68F9
    Length                  0x001C
    Revision                0x0003
    DeviceListOffset        0x00
    Class Code              0x000000
    Image size              0xFA00
    Code revision:          0x0000
    MaxRuntimeImageLength   0x00
    ConfigUtilityCodeHeaderOffset 0x00
    DMTFCLPEntryPointOffset 0x00
    Indicator               0x80   (last image)
    Code type               0x03   (EFI image)
  EFI ROM header contents
    EFI Signature          0x0EF1
    Compression Type       0x0001 (compressed)
    Machine type           0xAA64 (AA64)
    Subsystem              0x000B (EFI boot service driver)
    EFI image offset       0x0038 (@0x38)

```

## radeon-5450-arm64.rom.txt

```
Image 1 -- Offset 0x0
  ROM header contents
    Signature              0xAA55
    PCIR offset            0x01EC
    Signature               PCIR
    Vendor ID               0x1002
    Device ID               0x68F9
    Length                  0x0018
    Revision                0x0000
    DeviceListOffset        0x00
    Class Code              0x030000
    Image size              0x10000
    Code revision:          0x0C14
    MaxRuntimeImageLength   0x00
    ConfigUtilityCodeHeaderOffset 0x5441
    DMTFCLPEntryPointOffset 0x2049
    Indicator               0x00
    Code type               0x00
Image 2 -- Offset 0x10000
  ROM header contents
    Signature              0xAA55
    PCIR offset            0x001C
    Signature               PCIR
    Vendor ID               0x1002
    Device ID               0x68F9
    Length                  0x001C
    Revision                0x0003
    DeviceListOffset        0x00
    Class Code              0x000000
    Image size              0xFA00
    Code revision:          0x0000
    MaxRuntimeImageLength   0x00
    ConfigUtilityCodeHeaderOffset 0x00
    DMTFCLPEntryPointOffset 0x00
    Indicator               0x80   (last image)
    Code type               0x03   (EFI image)
  EFI ROM header contents
    EFI Signature          0x0EF1
    Compression Type       0x0001 (compressed)
    Machine type           0xAA64 (AA64)
    Subsystem              0x000B (EFI boot service driver)
    EFI image offset       0x0038 (@0x10038)
```
