# Proxy settings for Docker
https://docs.docker.com/network/proxy/

# Windows Terminal handy to use for development with WSL2 and other shells
https://docs.microsoft.com/en-us/windows/terminal/install

# Export a custom WSL2 distro from a container image

https://docs.microsoft.com/en-us/windows/wsl/use-custom-distro

## Open a new WSL2 shell after running containerized example

### Create a storage directory in our Windows filesystem to store custom WSL2 distros
`examples/edk2$ mkdir -p /mnt/c/users/username/wslDistroStorage`

### Build a container image to use for exporting
`examples/edk2$ time docker-compose -f edk2_img.yml build`

```
real    3m46.985s
```

### Run a shell using the new ampere.edk2.img to have a running ampere.edk2.cntnr to export
`examples/edk2$ docker run --rm --name ampere.edk2.cntnr -d ampere.edk2.img /bin/sh -c "tail -f /dev/null"`

### Capture the ampere.edk2.cntnr ID in a var
`examples/edk2$ dockerContainerID=$(docker container ls -a | grep -i ampere.edk2.cntnr | awk '{print $1}')`

### Change to wslDistroStorage located in Windows home directory
`examples/edk2$ cd ../../../../wslDistroStorage/`

### Export ampere.edk2.cntnr to an archive for WSL2 importing
`~/wslDistroStorage$ time docker export $dockerContainerID | gzip > ampere.edk2.cntnr.tgz`

```
real    2m5.154s
```

### Remove the running ampere.edk2.cntnr we only needed for export purposes
`~/wslDistroStorage$ docker rm -f ampere.edk2.cntnr`

# Import a custom WSL2 distro from a container archive (ampere.edk2.cntnr.tgz)

## Configure WSL automounts to access Windows filesystem in WSL distros through /mnt/c/...

https://docs.microsoft.com/en-us/windows/wsl/wsl-config

### Windows PowerShell Configure WSL2 Mounts

`C:\Users\username> notepad.exe .\.wslconfig`

### Enable extra metadata options by default
```
[automount]
enabled = true
root = /mnt/
options = "metadata,umask=22,fmask=11"
mountFsTab = false
```

### Change to our WSL2 storage place for our custom distros
`PS C:\Users\username> cd .\wslDistroStorage\`

### Import the containerized image we exported
`PS C:\Users\username\wslDistroStorage> Measure-Command { wsl --import ampere.edk2.centos AmpEdkCentOS ampere.edk2.cntnr.tgz }`

```
TotalSeconds      : 13.3069363
```

# Display distros
`PS C:\Users\username> wsl -l -v`

# Create a Windows casesensetive build directory for the Ampere EDK2 Sources
`PS C:\Users\username> mkdir -p C:\Users\username\Source\ampere_edk2_build`

### Enable casesensitive Windows shared directory to build Linux sources so make won't error out with duplicate names
`PS C:\Users\username> fsutil.exe file SetCaseSensitiveInfo C:\Users\username\Source\ampere_edk2_build enable`

```
Case sensitive attribute on directory c:\users\username\Source\ampere_edk2_build is enabled.
```
### Change to new ampere_edk2_build directory
`cd Source\ampere_edk2_build`

### Shutdown existing WSL2 instances to reread config to automount local directories
`PS Source\ampere_edk2_build> wsl --shutdown`

### Run a WSL2 version of our exported Docker example
`PS Source\ampere_edk2_build> wsl -d ampere.edk2.centos`

___
### OPTIONAL UPDATE PROXY FOR NEW/CUSTOM WSL2 DISTRO

#### Git Proxy
`nano /root/.gitconfig`

```
# [http]
#         proxy = http://myproxy.mydomain.com:8080
# [https]
#         proxy = https://myproxy.mydomain.com:8080
```
*Alternative method using Git command vs. nano*

`git config --global http.proxy http://myproxy.mydomain.com:8080`

`git config --global https.proxy https://myproxy.mydomain.com:8080`
#### Global Proxy
`nano /etc/profile`

```
# PROXY_URL="http://myproxy.mydomain.com:8080"
# export http_proxy="$PROXY_URL"
# export https_proxy="$PROXY_URL"
# export ftp_proxy="$PROXY_URL"
# export no_proxy="127.0.0.1,localhost"

# Proxy settings for curl
# export HTTP_PROXY="$PROXY_URL"
# export HTTPS_PROXY="$PROXY_URL"
# export FTP_PROXY="$PROXY_URL"
# export NO_PROXY="127.0.0.1,localhost"
```

#### Wget Proxy
`nano /etc/wgetrc`

```
#https_proxy = http://myproxy.mydomain.com:8080/
#http_proxy = http://myproxy.mydomain.com:8080/
#ftp_proxy = http://myproxy.mydomain.com:8080/
#use_proxy = on
```

### Exit container to get proxy settings upon next startup
`exit`

### Run a WSL2 version of our exported Docker example
`PS Source\ampere_edk2_build> wsl -d ampere.edk2.centos`
___

### Extract the ATF_SLIM, BOARD_SETTING, SCP_SLIM from the SRP archive
`tar -xf ../../LocalDownload/srp-ac01-fw-aptv-bin-r1.08b.20211015.tar.xz srp-ac01-fw-aptv-bin-r1.08b.20211015/altra_firmware_sdk/bin/atf/altra_atf_signed_1.08.20211002.slim srp-ac01-fw-aptv-bin-r1.08b.20211015/altra_firmware_sdk/bin/board_settings/jade_board_setting_1.08.20211002.bin srp-ac01-fw-aptv-bin-r1.08b.20211015/altra_firmware_sdk/bin/scp/altra_scp_signed_1.08.20211002.slim --one-top-level=srp --strip-components=4`

### Cloning EDK2 source code repos
`time git clone --recurse-submodules --branch ampere https://github.com/AmpereComputing/edk2.git`

```
real    6m7.046s
```

`time git clone --branch ampere https://github.com/AmpereComputing/edk2-non-osi.git`

```
real    0m7.027s
```

`time git clone --branch master https://github.com/AmpereComputing/edk2-ampere-tools.git`

```
real    0m0.807s
```

`time git clone --recurse-submodules --branch ampere https://github.com/AmpereComputing/edk2-platforms.git`

```
real    1m21.223s
```

`time git clone --branch master https://github.com/linuxboot/mainboards.git`

```
real    0m2.271s
```

### Build LinuxBoot mainboards/ampere/jade
`time make -C mainboards/ampere/jade fetch flashkernel ARCH=arm64 CROSS_COMPILE=/root/build/ampere-8.3.0-20191025-dynamic-nosysroot/bin/aarch64-ampere-linux-gnu-`

```
real    27m46.003s
```

### Build tianocore_img, tianocore_capsule, linuxboot_img
`cd edk2-ampere-tools && time make all VER=1.08 BUILD=100 ATF_SLIM=../srp/altra_atf_signed_1.08.20211002.slim BOARD_SETTING=../srp/jade_board_setting_1.08.20211002.bin SCP_SLIM=../srp/altra_scp_signed_1.08.20211002.slim LINUXBOOT_BIN=../mainboards/ampere/jade/flashkernel`

```
real    13m39.637s
real    89m49.110s
```

### Examine build artifacts tianocore_img, tianocore_capsule, linuxboot_img
`tree BUILDS/`
